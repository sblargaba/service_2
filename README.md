Service 2
=========

In this repository is the main `.gitlab-ci.yaml` file, which performs the given task on a merge request
1. The first step builds the image via kaniko using the provided dummy Dockerfile and tagging it with the project MR number
2. The second step uses the argo cli to deploy the provided helm chart, which includes
	- the Service 2 image built in the previous step
	- the Service 1 latest image
   A gitlab environment is created with the same URL that is also used in the virtualservice to route traffic
3. The third step is used to delete the environment when the merge request is closed



Further questions
-----------------
> Give an overview of how you would distribute access rights to the k8s cluster (forDevOps engineers, infrastructure engineers, frontend developers, backend developers,etc.), including access rights to kubectl, ArgoCD, Gitlab repositories and k8s nodes

I would give following rights:

**For the kubernetes clusters:**
- BE/FE developers:
	- prod cluster: read access in the team namespace to all resources except secrets, read access to all cluster-wide resources - this provides maximum transparency and helps them to troubleshoot any problem the application may face in production
	- dev cluster: read/write access in the team namespace to all resources, read access to all cluster-wide resrouces - this enables every member of the development team to expreiment within their own team's constraints
- DevOps engineers:
	- same right as developers, except on production they should be able to see the secrets within their teams' namespaces on production
- Infrastructure engineers:
	- full access to the cluster. Ideally never used, but needed for troubleshooting

**ArgoCD:**
To ensure the repository is the single source of truth, auto-sync and self-heal should be enabled, with pruning disabled to avoid accidental deletion of production components.
All configuration of ArgoCD applications and projects should be done via the relevant kubernetes CRD, so the ArgoCD web interface should provide read only access to inform users of the current sync status. Exception to this are:
- DevOps engineers should be able to manually trigger refresh/resync to avoid having to wait for polling intervals when troubleshooting/prototyping
- DevOps engineers should be able to delete resources within an application (as pruning is disabled)
- Infrastructure engineers should have full access

**Repositories:**
For each project, prevent direct writes on the main branch upon which automation relies, only allowing merges approved by another teammate. 

This might be cumbersome for small side projects so it might not be enforced by default: a monthly audit should remind of which repositories are not secured, in order to keep track of exceptions made

---

>Given it should be simple for developers to push to production but also to easily rollback to the previous version without having developers manually edit files onkubernetes (so no ability for them to deploy other apps), how would you solve that?

A proper rollback strategy relies on several important factors:
1. Proper tagging of the images deployed (and a strategy to store said images)
2. Explicit reference and version pinning of all resources used (helm charts, container images, etc)
3. The usage of a GitOps tool (such as ArgoCD/Flux) that ensures the repository is the single source of truth on what's deployed at any given time

If these conditions are satisfied, then performing a rollback becomes a trivial operation of identifying the commit reflecting a working application state and reverting all changes between that commit and the last one referenced by the continous delivery tool.
This revert should be implemented as a new commit to the repository, which will be then picked up by the CD tool which will attempt to reconcile the status of the cluster, effectively rolling back the problematic changes.

This allows to prevent any manual interaction with the production cluster itself, using Git instead to manipulate the state of the cluster, leaving an audit trail and enforcing peer review practices, as well as removing any guesswork from the rollback process.


